package com.example.demo.controller;

import java.net.http.HttpHeaders;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employee;
import com.example.demo.service.EmployeeService;

@RestController
@RequestMapping("/employees")
public class EmployeeControler {
	@Autowired
	EmployeeService employeeService;
	
	@GetMapping
	public ResponseEntity<List<Employee>> getAllEmployee(
			 @RequestParam(defaultValue = "0") Integer pageNo, 
             @RequestParam(defaultValue = "1") Integer pageSize,
             @RequestParam(defaultValue = "id") String sortBy){
		
		List<Employee> list = employeeService.getAllEmployees(pageNo, pageSize, sortBy);
		
		return new ResponseEntity<List<Employee>>(list,HttpStatus.OK);		
	}
	@GetMapping("/getemployee")
	public List<Employee> getMapping(){
		return employeeService.getMapping();
	}
	@PostMapping("/employee1")
	public Employee postEmployee(@RequestBody Employee emp) {
		return employeeService.postEmployee(emp);
	}
	@PutMapping("emp2/{id}")
	public Employee updateEmployee(@PathVariable("id") long id,@RequestBody Employee emp) {
		return employeeService.updateEmployee(id,emp);
	}
	@DeleteMapping("/employee/{id}")
	public void deleteEmployee(@PathVariable("id") long id)
	{
		employeeService.deleteEmployee(id);
	}
}
