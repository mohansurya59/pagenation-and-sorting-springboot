package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;

@Service
public class EmployeeService {
@Autowired
EmployeeRepository employeeRepository;

	public List<Employee> getAllEmployees(Integer pageNo, Integer pageSize, String sortBy) {
		PageRequest pagging=PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		
		Page<Employee> pagedResult=employeeRepository.findAll(pagging);
		
		 if(pagedResult.hasContent()) {
	         return pagedResult.getContent();
	     } else {
	         return new ArrayList<Employee>();
	     }
	   }
    public List<Employee> getMapping() {
		
		return employeeRepository.findAll();
	}

	public Employee postEmployee(Employee emp) {
		
		return employeeRepository.save(emp);
	}

	public Employee updateEmployee(long id, Employee emp) {
       Employee emp1=employeeRepository.getOne(id);
		
		emp1.setFirstName(emp.getFirstName());
		emp1.setLastName(emp.getLastName());
		emp1.setEmail(emp.getEmail());
		return employeeRepository.save(emp1);
	}

	public void deleteEmployee(long id) {
		employeeRepository.deleteById(id);
	}

		
	
}
